const initialValue = document.getElementById("initialValue");
const output = document.getElementById("output");
const convert = document.getElementById("convert");
const select1 = document.getElementById("selectFrom");
const select2 = document.getElementById("selectTo");
let accept = document.getElementById("accept");
const selectLang = document.getElementById("lang");
const container = document.getElementById("mainContainer");

accept.addEventListener("click", changeLanguage);
convert.addEventListener("click", getConvertValue);

let metersObj = {
    "yards": 0.9144,
    "mile": 1609.34,
    "foot": 0.3048,
    "verst": 1066.8
};

let vocabulary = {
    "english": [
        "Change localization",
        "Accept",
        "Input",
        "Verst",
        "Yards",
        "Mile",
        "Foot",
        "Convert",
        "Output",
        "Verst",
        "Yards",
        "Mile",
        "Foot",
    ],
    "russian": [
        "Изменить язык",
        "Применить",
        "Ввод",
        "Версты",
        "Ярды",
        "Мили",
        "Футы",
        "Конвертировать",
        "Вывод",
        "Версты",
        "Ярды",
        "Мили",
        "Футы",
    ],
    "arabian": [
        "اللغة",
        "للتقديم",
        "دخول",
        "معالم",
        "ياردة",
        "ميل",
        "قدم",
        "تحويل",
        "استنتاج",
        "معالم",
        "ياردة",
        "ميل",
        "قدم"
    ]
};

function changeLanguage() {
    changeTextPage(checkForVocabulary());
}

function checkForVocabulary() {
    let stringLang = currentLang();
    console.log(stringLang);

    let result;
    switch (stringLang) {
        case "English":
            result = vocabulary.english;
            container.classList.remove("main-container") ;
            break;
        case "Русский":
            result = vocabulary.russian;
            container.classList.remove("main-container") ;
            break;
        default :
            result = vocabulary.arabian;
            container.classList.add("main-container") ;
            break;
    }

    return result;
}

function currentLang() {
    var i = selectLang.selectedIndex;
    return selectLang.options[i].text;
}

function changeTextPage(langLibrery) {
    langLocal = document.getElementsByClassName("local");
    console.log(langLocal);
    let counterLang = 0;

    for (let key in langLocal) {
        if (langLocal.hasOwnProperty(key)) {
            langLocal[key].innerText = langLibrery[counterLang];
        }
        counterLang++;
    }
}

function getConvertValue() {
    let options1 = select1.options[select1.selectedIndex].innerText;
    let options2 = select2.options[select2.selectedIndex].innerText;
    output.value = secondConvert(options1, options2);
}

function firstConvert(options1) {
    let inputValue = initialValue.value;
    console.log(inputValue);
    if (inputValue === false) {
        return output.value = "Enter a long";
    }
    return inputValue * metersObj[options1];
}

function secondConvert(options1, options2) {
    let convertFirst = firstConvert(options1);
    return convertFirst/metersObj[options2];
}

